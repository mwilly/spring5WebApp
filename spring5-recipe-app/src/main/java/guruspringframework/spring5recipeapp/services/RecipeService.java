package guruspringframework.spring5recipeapp.services;

import guruspringframework.spring5recipeapp.commands.RecipeCommand;
import guruspringframework.spring5recipeapp.domain.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();

    Recipe findById(Long l);

    RecipeCommand saveRecipeCommand(RecipeCommand command);
}
