package com.mattwilly.spring5webapp.bootstrap;

import com.mattwilly.spring5webapp.model.Author;
import com.mattwilly.spring5webapp.model.Book;
import com.mattwilly.spring5webapp.model.Publisher;
import com.mattwilly.spring5webapp.repositories.AuthorRepository;
import com.mattwilly.spring5webapp.repositories.BookRepository;
import com.mattwilly.spring5webapp.repositories.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    public void initData() {
        //Eric
        Author eric = new Author("Eric", "Evans");
        Publisher collins = new Publisher("Harper Collins", "221B Baker Street", "London", "UK", "1234");
        publisherRepository.save(collins);
        Book ddd = new Book("Domain Driven Design", "1234", collins);
        eric.getBooks().add(ddd);
        ddd.getAuthors().add(eric);

        authorRepository.save(eric);
        bookRepository.save(ddd);


        //Rod
        Author rod = new Author("Rod", "Johnson");
        Publisher worx = new Publisher("Worx", "2345 Rocky Ave.", "Denver", "Colorado", "84561");
        publisherRepository.save(worx);
        Book noEJB = new Book("J2EE Development without EJB", "23444", worx);
        noEJB.setPublisher(worx);
        rod.getBooks().add(noEJB);


        authorRepository.save(rod);
        bookRepository.save(noEJB);
    }
}
