package guru.springframework.didemo.services;

import org.springframework.stereotype.Component;

@Component
public class GreetingRepositoryImpl implements GreetingRepository {

    @Override
    public String getEnglishGreeting() {
        return "Hello Primary Greeting service";
    }

    @Override
    public String getSpanishGreeting() {
        return "Servicio de Salundo Primario";
    }

    @Override
    public String getGermanGreeting() {
        return "Grüß dich! Wilkommen";
    }
}
